package com.example.university_management.service;

public interface BaseService<T, U> {
    T save(U dtoObject);

    T update(U dtoObject);

    T findById(Long id);

    void deleteById(Long id);
}
