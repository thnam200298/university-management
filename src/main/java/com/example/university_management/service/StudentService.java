package com.example.university_management.service;

import com.example.university_management.model.request.StudentRequestDto;
import com.example.university_management.model.response.StudentResponseDto;

public interface StudentService extends BaseService<StudentResponseDto, StudentRequestDto>{
}
