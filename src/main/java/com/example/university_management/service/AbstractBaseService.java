package com.example.university_management.service;

public abstract class AbstractBaseService<T, U> implements BaseService<T, U> {
    @Override
    public T save(U dtoObject) {
        return null;
    }

    @Override
    public T update(U dtoObject) {
        return null;
    }

    @Override
    public T findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }
}
