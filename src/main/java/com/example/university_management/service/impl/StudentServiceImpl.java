package com.example.university_management.service.impl;

import com.example.university_management.entity.Student;
import com.example.university_management.model.request.StudentRequestDto;
import com.example.university_management.model.response.StudentResponseDto;
import com.example.university_management.model.response.mapper.StudentResponseMapper;
import com.example.university_management.repository.StudentRepository;
import com.example.university_management.service.AbstractBaseService;
import com.example.university_management.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class StudentServiceImpl extends AbstractBaseService<StudentResponseDto, StudentRequestDto> implements StudentService {
    private final StudentRepository studentRepository;
    private final StudentResponseMapper studentResponseMapper;

    @Override
    public StudentResponseDto findById(Long id) {
        Optional<Student> student = studentRepository.findById(1l);
        return studentResponseMapper.toDto(student.get());
    }
}
