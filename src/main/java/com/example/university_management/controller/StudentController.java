package com.example.university_management.controller;

import com.example.university_management.model.response.StudentResponseDto;
import com.example.university_management.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping(value = "{student_id}")
    public ResponseEntity<StudentResponseDto> get(
            @PathVariable("student_id") Long studentId) {
        return new ResponseEntity<>(studentService.findById(studentId), HttpStatus.OK);
    }
}
