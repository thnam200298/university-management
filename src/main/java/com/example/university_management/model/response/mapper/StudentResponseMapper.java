package com.example.university_management.model.response.mapper;

import com.example.university_management.entity.Student;
import com.example.university_management.model.response.StudentResponseDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StudentResponseMapper extends EntityMapper<StudentResponseDto, Student> {
}
